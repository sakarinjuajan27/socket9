package main

import (
	"fmt"
	constV1 "socket9/constants"
	"strconv"
	"strings"
)

func main() {

	var (
		day          int
		integerMonth int
		yearStart    int
		yearEnd      int
		err          error
		result       string
		dayOfMonth   int
	)

	input := "May 19 2000"

	output := strings.Split(input, " ")

	day, _ = strconv.Atoi(output[1])
	month := output[0]
	year, _ := strconv.Atoi(output[2])

	if year < 1900 {

		fmt.Println("Please input year more than or equal 1900")
		return

	}

	febLeapYear, stringLeapYear := checkLeapYear(year) //check leap year
	dayOfMonth, integerMonth = CheckDaysOfMonth(febLeapYear, month)
	if !(day >= 1 && day <= dayOfMonth) {
		fmt.Println("Out of range Day of month.")
		return
	}

	if month == "Jan" || month == "Feb" {
		year = year - 1
		yearStart, yearEnd, err = SplitYear(year)
		if err != nil {
			fmt.Println(err.Error())
			return
		}
	} else {

		yearStart, yearEnd, err = SplitYear(year)
		if err != nil {
			fmt.Println(err.Error())
			return
		}

	}

	result, err = CalculateWeekdayIsToday(day, integerMonth, yearStart, yearEnd)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println("This Year is " + stringLeapYear)
	fmt.Println(input + " is " + result)

}

func CalculateWeekdayIsToday(day, month, yearStart, yearEnd int) (string, error) {

	var output string
	var finalCal int
	monthCal := ((13 * month) - 1) / 5
	yearEndCal := (yearEnd / 4)
	yearStartCal := (yearStart / 4) - (2 * yearStart)

	sum := (day + monthCal + yearEnd + yearEndCal + yearStartCal)

	finalCal = sum

	if sum < 0 {

		for i := 0; finalCal < 0; i++ {
			finalCal = finalCal + 7
		}

	} else {
		finalCal = sum % 7
	}

	switch finalCal {

	case 0:
		output = constV1.Monday
	case 1:
		output = constV1.Monday
	case 2:
		output = constV1.Tuesday
	case 3:
		output = constV1.Wednesday
	case 4:
		output = constV1.Thursday
	case 5:
		output = constV1.Friday
	case 6:
		output = constV1.Saturday

	default:
		fmt.Println("out of days")
		return "", nil

	}

	return output, nil
}

func SplitYear(year int) (int, int, error) {
	numberOfYear := strconv.Itoa(year)
	yearStart := numberOfYear[0:2]
	yearEnd := numberOfYear[2:4]

	intYearStart, err := strconv.Atoi(yearStart)
	if err != nil {
		fmt.Println(err.Error())
		return 0, 0, err
	}
	intYearEnd, err := strconv.Atoi(yearEnd)
	if err != nil {
		fmt.Println(err.Error())
		return 0, 0, err
	}

	return intYearStart, intYearEnd, err
}

func CheckDaysOfMonth(febLeapYear int, stringMonth string) (int, int) {

	dayOfMonth := 0
	month := 0

	switch stringMonth {
	case "Jan":
		month = constV1.January

		dayOfMonth = constV1.ThirtyOneDays

	case "Feb":
		month = constV1.February
		dayOfMonth = febLeapYear

	case "Mar":
		month = constV1.March
		dayOfMonth = constV1.ThirtyOneDays

	case "Apr":
		month = constV1.April
		dayOfMonth = constV1.ThirtyDays

	case "May":
		month = constV1.May
		dayOfMonth = constV1.ThirtyOneDays

	case "Jun":
		month = constV1.June
		dayOfMonth = constV1.ThirtyDays

	case "Jul":
		month = constV1.July
		dayOfMonth = constV1.ThirtyOneDays

	case "Aug":
		month = constV1.August
		dayOfMonth = constV1.ThirtyOneDays

	case "Sep", "Sept":
		month = constV1.September
		dayOfMonth = constV1.ThirtyDays

	case "Oct":
		month = constV1.October
		dayOfMonth = constV1.ThirtyOneDays

	case "Nov":
		month = constV1.November
		dayOfMonth = constV1.ThirtyDays

	case "Dec":
		month = constV1.December
		dayOfMonth = constV1.ThirtyOneDays
	default:
		fmt.Println("out of months")
		return 0, 0

	}

	return dayOfMonth, month

}

func checkLeapYear(year int) (int, string) {

	febLeapYear := constV1.FebOnly
	stringLeapYear := "Not a leap year"

	if year%4 != 0 {
		febLeapYear = constV1.FebOnly //not a leap year
		stringLeapYear = "Not a leap year"
	}
	if year%4 == 0 && year%100 != 0 && year%400 != 0 {
		febLeapYear = 29 //leap year
		stringLeapYear = "Leap year"
	}

	if year%4 == 0 && year%100 == 0 && year%400 == 0 {

		febLeapYear = 29 //leap year
		stringLeapYear = "Leap year"
	}

	if year%4 == 0 && year%100 == 0 && year%400 != 0 {
		febLeapYear = constV1.FebOnly //not a leap year
		stringLeapYear = "Not a leap year"
	}

	return febLeapYear, stringLeapYear

}
