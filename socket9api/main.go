package main

import (
	socket9api "socket9api/dayofweek"

	"github.com/gin-gonic/gin"
)

func main() {

	r := gin.Default()

	r.POST("/weekday", socket9api.CalculateWeekday)

	r.Run("localhost:8000")
}
