package constV1

const (
	Sunday    = "Sunday"
	Monday    = "Monday"
	Tuesday   = "Tuesday"
	Wednesday = "Wednesday"
	Thursday  = "Thursday"
	Friday    = "Friday"
	Saturday  = "Saturday"
)

const (
	ThirtyDays    = 30
	ThirtyOneDays = 31
	FebOnly       = 28
)

const (
	January   = 11
	February  = 12
	March     = 1
	April     = 2
	May       = 3
	June      = 4
	July      = 5
	August    = 6
	September = 7
	October   = 8
	November  = 9
	December  = 10
)
